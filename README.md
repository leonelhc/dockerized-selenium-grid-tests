# README #

### Wikipedia Page Object Tests on Dockerized Selenium Grid

* Test project aimed at demonstrating the use of the Page Object design pattern in Java for the development of automated tests associated with a website or web application.
* Tests run in parallel on two browsers (Chrome/Firefox) using Selenium Grid and TestNG.
* The entire project is containerized with Docker.

It uses the Spanish Wikipedia site (https://es.wikipedia.org/wiki/Wikipedia:Portada) as a base example for testing.


**Instructions**

* Requirements: Docker / VNC Viewer (if you want to visualize the execution within the containers)
* To set up Selenium Grid and run the tests automatically, simply execute the following command in the root directory:

`docker-compose up`

* To view the test execution, once the nodes are running, connect using VNC Viewer to _localhost:5901_ y _localhost:5902_ (password: _secret_).


_Author: Leonel Calzón_