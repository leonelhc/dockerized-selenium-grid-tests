package WikiPages;

import org.bouncycastle.jcajce.provider.symmetric.ARC4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class WikipediaHomePage extends BasePage {

    @FindBy(name = "search")
    private WebElement boxBusqueda;

    @FindBy(id = "searchButton")
    private WebElement btnBusqueda;

    @FindBy(className = "suggestions-result")
    private List<WebElement> listSuggestions;

    public WikipediaHomePage(WebDriver driver) {
        super(driver);
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public String getCurrentURL(){
        return driver.getCurrentUrl();
    }

    public WikipediaArticlePage buscarArticulo(String articulo){
        this.wait.until(ExpectedConditions.elementToBeClickable(boxBusqueda));
        boxBusqueda.sendKeys(articulo);
        btnBusqueda.click();
        return new WikipediaArticlePage(driver);
    }

    public List<String> listaDeSuggestions (String textoABuscar){
        this.wait.until(ExpectedConditions.elementToBeClickable(boxBusqueda));
        boxBusqueda.clear();
        boxBusqueda.sendKeys(textoABuscar);

        List<String> listadoSugerencias = new ArrayList<>();

        this.wait.until(ExpectedConditions.visibilityOfAllElements(listSuggestions));
        for (WebElement sg: listSuggestions) {
            listadoSugerencias.add(sg.getText());
        }
        return listadoSugerencias;
    }
}
