package WikiPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.List;

public class WikipediaArticlePage extends BasePage {

    @FindBy(id = "toc")
    private WebElement indice;

    public WikipediaArticlePage(WebDriver driver) {
        super(driver);
    }
    public String getTitle(){
        return driver.getTitle();
    }

    public boolean existeIndice(){
        return (indice.getText().contains("Índice"));
    }
}
