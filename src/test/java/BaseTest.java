import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebDriverBuilder;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.System.setProperty;

import static java.lang.System.setProperty;

/**
 * Created by leonelcalzon on 19/01/17.
 */
public class BaseTest {

    private WebDriver driver = null;

    private String browser;

    private ChromeOptions chromeOptions;
    private FirefoxOptions firefoxOptions;

    private static String CHROMEDRIVER_DRIVER_PATH;
    private static String GECKODRIVER_DRIVER_PATH;
    private static String REMOTE_SERVER_URL;
    private static int IMPLICIT_TIMEOUT;
    private static int FLUENT_TIMEOUT;

    protected static final Logger LOGGER = Logger.getLogger(BaseTest.class.getName());


    public void setUpTestProperties() {
        LOGGER.setLevel(Level.INFO);

        CHROMEDRIVER_DRIVER_PATH = "src/main/resources/chromedriver.exe";
        GECKODRIVER_DRIVER_PATH = "src/main/resources/geckodriver.exe";

        System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_DRIVER_PATH);
        System.setProperty("webdriver.gecko.driver", GECKODRIVER_DRIVER_PATH);

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        try (InputStream resourceStream = loader.getResourceAsStream("test-config.properties")) {
            properties.load(resourceStream);

            REMOTE_SERVER_URL = properties.getProperty("remote.server.url");
            IMPLICIT_TIMEOUT = Integer.parseInt(properties.getProperty("implicit.wait.timeout"));
            FLUENT_TIMEOUT = Integer.parseInt(properties.getProperty("fluent.wait.timeout"));
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public BaseTest(String browser) throws MalformedURLException {

        setUpTestProperties();
        this.browser = browser;

        if (browser.equalsIgnoreCase("CHROME")){

           chromeOptions = new ChromeOptions();
           this.driver =  new RemoteWebDriver(new URL(REMOTE_SERVER_URL), chromeOptions);
           this.driver.manage().timeouts().implicitlyWait(IMPLICIT_TIMEOUT, TimeUnit.SECONDS);

           LOGGER.info("CHROMEDRIVER: TEST SETUP OK");
        } else if (browser.equalsIgnoreCase("FIREFOX")) {

            firefoxOptions = new FirefoxOptions();
            this.driver =  new RemoteWebDriver(new URL(REMOTE_SERVER_URL), firefoxOptions);
            this.driver.manage().timeouts().implicitlyWait(IMPLICIT_TIMEOUT, TimeUnit.SECONDS);

            LOGGER.info("GECKODRIVER: TEST SETUP OK");
        }
    }

    public WebDriver getDriver() {
        return this.driver;
    }

}
