import WikiPages.WikipediaArticlePage;
import WikiPages.WikipediaHomePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.util.List;

/**
 * Created by leonelcalzon on 22/12/18.
 */

public class WikipediaTest {

    private WebDriver driver;
    private static final String INITIAL_APPLICATION_URL = "https://es.wikipedia.org/wiki/Wikipedia:Portada";

    @BeforeClass(alwaysRun = true)
    @Parameters({"browser"})
    public void setUpTestDriver(@Optional("CHROME") String browser) throws MalformedURLException {
        BaseTest baseTestConfiguration = new BaseTest(browser);
        this.driver = baseTestConfiguration.getDriver();
        this.driver.get(INITIAL_APPLICATION_URL);
    }

    @Test(priority = 0)
    public void testTituloWiki() throws InterruptedException {
        WikipediaHomePage wikipage = new WikipediaHomePage(driver);
        Assert.assertEquals(wikipage.getTitle(), "Wikipedia, la enciclopedia libre");
    }

    @Test(priority = 1)
    public void testCurrentURL() throws InterruptedException {
        WikipediaHomePage wikipage = new WikipediaHomePage(driver);
        Assert.assertEquals(wikipage.getCurrentURL(), "https://es.wikipedia.org/wiki/Wikipedia:Portada");
    }

    @Test(priority = 2)
    public void argentinaEntrePrimerasSugerenciasLetraA() throws InterruptedException {
        WikipediaHomePage wikiHomePage = new WikipediaHomePage(driver);
        List<String> listadoSugerencias = wikiHomePage.listaDeSuggestions("A");
        Assert.assertTrue(listadoSugerencias.contains("Argentina"));
    }

    @Test(priority = 3)
    public void buscarArticulo(){
        String nombreArticulo = "San Lorenzo de Almagro";
        driver.navigate().refresh();
        WikipediaHomePage wikiHomePage = new WikipediaHomePage(driver);
        WikipediaArticlePage wikiResultadoPage = wikiHomePage.buscarArticulo(nombreArticulo);
        Assert.assertTrue(wikiResultadoPage.getTitle().contains(nombreArticulo));
    }

    @Test (priority = 4, dataProvider = "articulosABuscar", dataProviderClass = FactoryWikipediaTest.class, alwaysRun = true)
    public void buscarListadoDeArticulosDataProvider(String nombreArticulo){
        WikipediaHomePage wikiHomePage = new WikipediaHomePage(driver);
        WikipediaArticlePage wikiResultadoPage = wikiHomePage.buscarArticulo(nombreArticulo);
        Assert.assertTrue(wikiResultadoPage.getTitle().contains(nombreArticulo));
    }

    @AfterClass
    public void tearDownDriver() {
        try {
            driver.close();
            driver.quit();
        } catch (org.openqa.selenium.NoSuchSessionException ex){
            ex.getStackTrace();
        }
    }

}
