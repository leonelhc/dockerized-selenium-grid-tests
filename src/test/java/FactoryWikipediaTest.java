import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by leonelcalzon on 22/12/16.
 */
public class FactoryWikipediaTest {

    // Método para generar cinco ejecuciones de todos los test de la clase WikipediaTest
    @Factory
    public Object[] crearInstanciasTestTituloWiki(){
        Object[] result = new Object[5];
        for (int i = 0; i < 5; i++) {
            result[i] = new WikipediaTest();
        }
        return result;
    }

    @DataProvider (name = "articulosABuscar")
    public static Object[][] articulosABuscar() throws IOException {

        String filePath = "src/main/resources/listadoDeArticulosTest.csv";
        CSVParser csvParser = null;
        List<String > listadoDeArticulos = new ArrayList<>();

        try {
            csvParser = new CSVParser(new FileReader(filePath), CSVFormat.DEFAULT);
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord line : csvRecords) {
                listadoDeArticulos.add(line.get(0));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //Transformo la lista de artículos del csv en un Object[][], para poder retornar el tipo de dato que requiere el DataProvider.
        int cantidadTitulos = listadoDeArticulos.size();

        String[][] titulos = new String[cantidadTitulos][1];

        for (int i=0;  i < cantidadTitulos; i++){
            titulos[i][0] = listadoDeArticulos.get(i);
        }

        return titulos;
    }
}
