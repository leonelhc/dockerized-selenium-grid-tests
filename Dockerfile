FROM selenium/base

USER root

WORKDIR '/wikipediapageobject'

# update dpkg repositories
RUN sudo apt-get update -y

# install wget
RUN sudo apt-get install -y wget

#install default java jdk
RUN sudo apt-get install default-jdk -y

# get maven 3.6.0
RUN wget --no-verbose -O /tmp/apache-maven-3.6.0.tar.gz http://apache.dattatec.com/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz

# install maven
RUN tar xzf /tmp/apache-maven-3.6.0.tar.gz -C /opt/
RUN ln -s /opt/apache-maven-3.6.0 /opt/maven
RUN ln -s /opt/maven/bin/mvn /usr/local/bin
RUN rm -f /tmp/apache-maven-3.6.0.tar.gz
ENV MAVEN_HOME /opt/maven

# remove download archive files
RUN apt-get clean

COPY ./ ./

ENTRYPOINT ["mvn", "clean", "test"]